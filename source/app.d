﻿module app;

import std.stdio;
import std.range;

import iarray;

//switching between string and inplace string
//======string======
//enum ILEN = 0; alias string test_string; string inplace(T, ubyte sz=0)(const char[] s){return s.idup;} 
//bool isInplace(ubyte sz=0)(string s) { return false; }; 
//size_t toHash(string s){return typeid(string).getHash(&s);}
//======manual inplace string======
//enum ILEN = 31; alias IArray!(char, ILEN) test_string;
//======default inplace string======
enum ILEN = defaultInplaceLength!char; alias IArray!(char, ILEN) test_string;

void main(string[] args){
	import core.memory;
	import std.algorithm, std.file, gzip;

	uint[test_string] dict;
	size_t count;
	auto fname = args[1];
	if(fname.endsWith(".gz")){
		count = new GzipInputRange(fname).buildWordsCount(dict);	
	}else{
		count = File(fname).byChar.buildWordsCount(dict);
	}
	 
	GC.collect;

	auto idx = 0;
	auto inplaceCount = 0;
	foreach(pair; dict.byKeyValue){
		if(idx++ < 10) 
			writefln("%s = %s, %s", pair.key, pair.value, pair.key.toHash);
		if(pair.key.isInplace) inplaceCount++;
	}
	writefln("Done %s. Inplace: %s; total: %s; %s", count, inplaceCount, dict.length, 100.0f*inplaceCount/dict.length);
}


size_t buildWordsCount(R)(R range, ref uint[test_string] dict){
	import std.uni;
	import std.utf;

	char[] tmp;
	char[4] buf;
	size_t count = 0;

	while(!range.empty){
		dchar c = range.decodeFront.toLower;
		if(c.isAlpha){
			auto len = encode(buf, c);
			tmp ~= buf[0 .. len];
		}else addWord(tmp, dict, count);
	}
	addWord(tmp, dict, count);
	return count;
}

void addWord(ref char[] tmp, ref uint[test_string] dict, ref size_t count){
	if(!tmp.empty){
		auto key = tmp.inplace!(char, ILEN);
		dict[key] += 1;
		count++;
		tmp.length = 0;
	}
}

auto byChar(File file, size_t bufSize = 4096)
{
	struct FileRange{
		File.ByChunk chunkRange;
		ubyte[] chunk;

		this(File f)
		{
			chunk = new ubyte[bufSize];
			chunkRange = f.byChunk(chunk);
		}

		@property char front(){
			return cast(char)chunk[0];
		}
		void popFront(){
			chunk = chunk[1 .. $];
		}
		@property bool empty(){
			if(chunk.length==0 && !chunkRange.empty){
				chunk = chunkRange.front; 
				chunkRange.popFront; 
			}
			return chunk.length==0;
		}
	}
	return FileRange(file);
}


/+
Done 83163128. Inplace: 0; total: 5624511; 0
	Number of collections:  12
	Total GC prep time:  155 milliseconds
	Total mark time:  2960 milliseconds
	Total sweep time:  1496 milliseconds
	Total page recovery time:  1046 milliseconds
	Max Pause Time:  1436 milliseconds
	Grand total GC time:  5659 milliseconds
GC summary: 1355 MB,   12 GC 5659 ms, Pauses 3116 ms < 1436 ms

Done 83163128. Inplace: 5592138; total: 5624511; 99.4244
	Number of collections:  22
	Total GC prep time:  15 milliseconds
	Total mark time:  4192 milliseconds
	Total sweep time:  684 milliseconds
	Total page recovery time:  389 milliseconds
	Max Pause Time:  938 milliseconds
	Grand total GC time:  5281 milliseconds
GC summary:  715 MB,   22 GC 5281 ms, Pauses 4207 ms <  938 ms

 +/