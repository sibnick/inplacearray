module iarray;

debug import std.stdio;


auto defaultInplaceLength(T)() pure{
	import std.algorithm:max;
	return max( 1, (size_t.sizeof*3-1)/(T).sizeof );
}

auto defInplace(T)(const T[] items){
	return inplace!(T, defaultInplaceLength!T)(items);
}

IArray!(T,size) inplace(T, ubyte size=defaultInplaceLength!T())(const T[] items){
	static assert (size<128);
	static assert (size>=defaultInplaceLength!T());

	IArray!(T, size) t;
	t = items;
	if(!t.isInplace) {
		t.cachedHash = t.arr.hashOf;
		t.iarr.status.inplace = false;
	}
	return t;
}

struct IArray(T, ubyte size=defaultInplaceLength!T()){
	static assert (size<128);
	static assert (size>=defaultInplaceLength!T());
	import std.conv;
	pragma(msg, "size: "~to!string(size));

	private union{
		InplaceArray!(T, size) iarr  = {status: Status(1)};
		
		struct {
			T[] arr;// = void;
			size_t cachedHash;
		}
	}
	
public: 
	void reset(){
		iarr.data[] = T.init;
	}
	
	@property bool isInplace() pure const nothrow{
		return this.iarr.status.inplace;
		//import core.bitop;
		//return 0!=bt(this.iarr.a.ptr + 2, 63);
	}
	
	@property size_t length() const{
		return isInplace ? this.iarr.status.length : this.arr.length;
	}
	
	//alias length opDollar;
	
	@property void length(size_t length){
		if(iarr.status.inplace){
			if(length<=iarr.data.length){
				iarr.status.length = length;
				for(auto i = iarr.status.length; i<length; i++){
					iarr.data[i] = T.init;
				}
			}else{
				T[] tmp = new T[length];
				for(auto i=0; i<iarr.status.length; i++){
					tmp[i] = iarr.data[i];
				}
				arr = tmp;
			}
		}else{	
			if(length<=iarr.data.length){
				T[] tmp = arr;
				tmp.length = length;
				iarr.status.length = length;
				iarr.status.inplace = true;
				foreach(i, t; tmp){
					iarr.data[i] = t;
				}
			}else{
				arr.length = length;
			}
		}
	}
	
	@property bool empty() const
	{
		return !this.length;
	}
	
	@disable void opBinary(string op : "~")(T t){}
	@disable void opBinary(string op : "~")(T[] t){}

	T opIndex(size_t idx)
	{
		if(isInplace){
			return iarr.data[idx];
		}else{
			return arr[idx];
		}
	}

	const T[] opSlice() {
		if(isInplace){
			auto length = iarr.status.length;
			return cast(T[])iarr.data[0 .. length];
		}else{
			return cast(T[])arr[];
		}
	}

	const T[] opSlice(size_t a, size_t b) 
	{
		if(isInplace){
			return cast(T[])iarr.data[a .. b];
		}else{
			return cast(T[])arr[a .. b];
		}
	}

	auto opAssign(const T[] items){
		if(items.length<=iarr.data.length){
			iarr.status.inplace = true;
			iarr.status.length = items.length;
			foreach(i, t; items){
				iarr.data[i] = t;
			}
		}else{
			cachedHash = 0;
			arr = cast(T[])items.dup;
		}
		return this;
	}
	alias opSlice this;

	T[] opAssign(string op : "~")(const T[] items){
		auto start = length();
		if(items.length+start<=iarr.data.length){
			iarr.status.inplace = true;
			iarr.status.length += items.length;
			foreach(i, t; items){
				iarr.data[i + start] = t;
			}
		}else{
			arr.length += items.length;
			foreach(i, t; items){
				arr[i + start] = t;
			}
		}
		return this;
	}
	
	T opAssign(string op : "~")(const T item){
		auto start = length();
		if(length+1<=iarr.data.length){
			iarr.status.inplace = true;
			iarr.status.length += 1;
			iarr.data[start] = t;
		}else{
			arr ~= item;
		}
		return this;
	}
	
	auto toString() {
		auto s = isInplace? iarr.data[0 .. length()] : arr;
		import std.conv:to;
		return to!string(s.idup);
	}

	
	size_t toHash() const @trusted nothrow{
		if(isInplace) {
			return this.iarr.status.length==0? 0 : iarr.data.hashOf;
		}else{
			return cachedHash;
		}
	}
	
	bool opEquals(ref const IArray!(T,size) s) const{
		if(isInplace && s.isInplace) {
			return s.iarr.data==iarr.data;
		}else if(!isInplace && !isInplace){
			if(s.cachedHash!=this.cachedHash) return false;
			return arr.ptr is null ? false : (arr is null)? false : arr==s.arr;
		}
		return false;
	}

}

private{
	struct DArr(T){
		size_t length;
		T* p;
	}

	struct Status
	{
		import std.bitmanip;
		mixin(bitfields!(
				size_t,  "length",    7,
				bool, "inplace",    1
				));
	}
	union InplaceArray(T, ubyte size){
		static assert ((T).sizeof<=size_t.sizeof);
		static assert ((InplaceArray!(T, size)).sizeof>=3*size_t.sizeof);
		
		align (1): struct{
			T[size] data;
			Status status;
		}
	}
	

}


unittest{
	auto arr = "abc".inplace;
	assert(arr.isInplace);
	arr = "12345678901234567890123".inplace;
	assert(arr.isInplace);
	arr = "123456789012345678901234".inplace;
	assert(!arr.isInplace);
	assert(arr.toString, "123456789012345678901234");
}

unittest{
	import std.algorithm;
	import std.stdio;
	import std.array;

	auto words = ["c", "b", "a"]
		.map!(a => a.inplace)
				.array
				.sort()
				.map!(a => a.toString);
	assert(words[0]=="a");
}

unittest{
	import std.algorithm;
	import std.stdio;
	import std.array;
	
	auto words = ["c", "b", "a"]
		.map!(a => [a.inplace, a.inplace])
		.join
			.uniq
			.array
			.sort();
	assert(words[0][]=="a");
}
